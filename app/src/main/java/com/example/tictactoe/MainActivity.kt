package com.example.tictactoe

import android.os.Bundle
import android.view.View
import android.content.Intent
import android.view.ViewAnimationUtils
import com.google.android.material.snackbar.Snackbar

import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun start_multi_player_game(view: View) {
        val intent = Intent(this, GameMultiPlayer::class.java)
        startActivity(intent)
    }
    fun start_single_player_game(view: View) {
        val intent = Intent(this, GameActivity::class.java)
        startActivity(intent)
    }

    fun open_hall_of_fame(view: View) {
        val intent = Intent(this, HallOfFame::class.java)
        startActivity(intent)
    }
}
