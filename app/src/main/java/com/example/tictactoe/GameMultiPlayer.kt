package com.example.tictactoe

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.service.quicksettings.Tile
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import org.w3c.dom.Text

class TickTackToeMultiplayer(
    private val game_board: Board,
    private val snackbar_target: TextView,
    private val sharedPreferences: SharedPreferences
) :
    TickTacToeBoardAdapter(game_board, snackbar_target, sharedPreferences) {
    var current_player = TileState.CROSS

    fun update_score() {
        val new_score = "X ${score_x}:${score_o} O"
        this.snackbar_target.text = new_score
        sharedPreferences.edit().putString("VS_PLAYER_SCORE", new_score).commit()
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        set_tile_icon(position, viewHolder)

        viewHolder.board_tile.setOnClickListener(View.OnClickListener {
            if (game_board.try_get_winner() != TileState.NONE || game_board.is_draw()) {
                game_board.restart()
                notifyDataSetChanged()
            } else {

                val (move_successful, message) = this.game_board.try_making_move(
                    position,
                    this.current_player
                )
                var snackbar = Snackbar.make(
                    this.snackbar_target,
                    message,
                    Snackbar.LENGTH_SHORT
                )

                if (move_successful) {
                    snackbar.setBackgroundTint(Color.BLUE)
                    this.current_player =
                        if (current_player == TileState.CIRCLE) TileState.CROSS else TileState.CIRCLE
                } else {
                    snackbar.setBackgroundTint(Color.RED)
                }

                snackbar.show()
                this.notifyDataSetChanged()

                val winner = this.game_board.try_get_winner()
                if (winner == TileState.CROSS) {
                    Snackbar.make(
                        this.snackbar_target,
                        "Cross won! Click on the board to restart",
                        Snackbar.LENGTH_SHORT
                    ).show()
                    score_x++
                    update_score()
                } else if (winner == TileState.CIRCLE) {
                    Snackbar.make(
                        this.snackbar_target,
                        "Circle won! Click on the board to restart",
                        Snackbar.LENGTH_SHORT
                    ).show()
                    score_o++
                    update_score()
                } else if (game_board.is_draw()) {

                    Snackbar.make(
                        this.snackbar_target,
                        "Draw! Click on the board to restart",
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            }
        })
    }

    override fun getItemCount() = game_board.num_of_tiles
}


class GameMultiPlayer : AppCompatActivity() {

    fun createGameBoard() {

        val gameBoardLayout = findViewById<RecyclerView>(R.id.game_board)
        val board = Board()
        val preferences =
            applicationContext.getSharedPreferences("HALL_OF_FAME", Context.MODE_PRIVATE)
        val adapter =
            TickTackToeMultiplayer(board, findViewById(R.id.current_game_score), preferences)

        gameBoardLayout.adapter = adapter
        gameBoardLayout.layoutManager = GridLayoutManager(this, 10)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        findViewById<LinearLayout>(R.id.gameLinearLayout).orientation = newConfig.orientation
//         newConfig.orientation
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        findViewById<LinearLayout>(R.id.gameLinearLayout).orientation =
            resources.configuration.orientation
        createGameBoard()
    }

}
