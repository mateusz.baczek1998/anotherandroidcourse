package com.example.tictactoe

enum class TileState {
    NONE,
    CROSS,
    CIRCLE
}

class Board {
    private val row = 10
    private val column = 10
    private val game_board =
        Array<Array<TileState>>(row) { Array<TileState>(column, { i -> TileState.NONE }) }

    val num_of_tiles = row * column

    fun try_get_winner(): TileState {
        var winner = try_get_winner_rows()

        if (winner == TileState.NONE) {
            winner = try_get_winner_columns()

            if (winner == TileState.NONE) {
                winner = try_get_winner_across_board()
            }
        }

        return winner
    }

    fun restart() {
        for (i in (0..9)) {
            for (j in (0..9)) {
                this.game_board[i][j] = TileState.NONE
            }
        }
    }

    // Is draw even possible here?
    fun is_draw(): Boolean {
        for (i in (0..9)) {
            for (j in (0..9)) {
                if (this.game_board[i][j] == TileState.NONE) {
                    return false
                }
            }
        }
        return true
    }

    private fun try_get_winner_across_board(): TileState {
        // Diagonal
//        Log.e("BOARD", "Checking diagonal")
        for (i in (0..9)) {
            for (j in (0..9)) {
                if (game_board[i][j] == TileState.NONE) {
                    continue
                }

                var cursor_i = i
                var cursor_j = j
                while (cursor_i > 0 && cursor_j > 0) {
                    cursor_i--
                    cursor_j--
                }

                var segment_length = 1
                var segment_type = game_board[cursor_i][cursor_j]

                while (cursor_i < 8 && cursor_j < 8) {
                    cursor_i++
                    cursor_j++

                    if (game_board[cursor_i][cursor_j] == TileState.NONE) {
                        segment_length = 0
                    } else if (game_board[cursor_i][cursor_j] == segment_type) {
                        segment_length++
                        if (segment_length == 5) {
                            return segment_type
                        }
                    } else {
                        segment_type = game_board[cursor_i][cursor_j]
                        segment_length = 1
                    }
                }

            }
        }
        // Anti diagonal
        for (i in (0..9)) {
            for (j in (0..9)) {
                if (game_board[i][j] == TileState.NONE) {
                    continue
                }

                var cursor_i = i
                var cursor_j = j

                while (cursor_i < 9 && cursor_j > 0) {
                    cursor_i++
                    cursor_j--
                }

                var segment_length = 1
                var segment_type = game_board[cursor_i][cursor_j]

                while (cursor_i > 1 && cursor_j < 9) {
                    cursor_i--
                    cursor_j++

                    if (game_board[cursor_i][cursor_j] == TileState.NONE) {
                        segment_length = 0
                    } else if (game_board[cursor_i][cursor_j] == segment_type) {
                        segment_length++
                        if (segment_length == 5) {
                            return segment_type
                        }
                    } else {
                        segment_type = game_board[cursor_i][cursor_j]
                        segment_length = 1
                    }
                }

            }
        }

//        return TileState.NONE

        for (i in (0..9)) {
            for (j in (0..9)) {
                if (game_board[i][j] == TileState.NONE) {
                    continue
                }

                var cursor_i = i
                var cursor_j = j

                while (cursor_i < 9 && cursor_j < 9) {
                    cursor_i++
                    cursor_j++
                }

                var segment_length = 1
                var segment_type = game_board[cursor_i][cursor_j]

                cursor_i++
                cursor_j++
                while (cursor_i > 1 && cursor_j > 1) {
                    cursor_i--
                    cursor_j--

                    if (game_board[cursor_i][cursor_j] == TileState.NONE) {
                        segment_length = 0
                    } else if (game_board[cursor_i][cursor_j] == segment_type) {
                        segment_length++
                        if (segment_length == 5) {
                            return segment_type
                        }
                    } else {
                        segment_type = game_board[cursor_i][cursor_j]
                        segment_length = 1
                    }
                }

            }
        }



        return TileState.NONE;
    }

    private fun try_get_winner_columns(): TileState {
        var segment_length = 0
        var segment_type = TileState.NONE

        for (column in 0..9) {
            for (row in (0..9)) {
                if (game_board[row][column] == TileState.NONE) {
                    segment_length = 0
                    continue
                }
                if (game_board[row][column] == segment_type) {
                    segment_length++
                    if (segment_length == 5) {
                        return segment_type
                    }
                } else {
                    segment_length = 1
                    segment_type = game_board[row][column]
                }
            }
        }

        return TileState.NONE

    }

    private fun try_get_winner_rows(): TileState {
        var segment_length = 0
        var segment_type = TileState.NONE

        for (row in 0..9) {
            for (column in (0..9)) {
                if (game_board[row][column] == TileState.NONE) {
                    segment_length = 0
                    continue
                }
                if (game_board[row][column] == segment_type) {
                    segment_length++
                    if (segment_length == 5) {
                        return segment_type
                    }
                } else {
                    segment_length = 1
                    segment_type = game_board[row][column]
                }
            }
        }

        return TileState.NONE
    }

    private fun unflatten_index(index: Int): Pair<Int, Int> {
        return Pair(index / 10, index % 10)
    }

    fun getTileAtFlatIndex(index: Int): TileState {
        val (index_y, index_x) = this.unflatten_index(index)
        return game_board[index_y][index_x]
    }

    fun try_making_move(index: Int, move: TileState): Pair<Boolean, String> {
        if (move == TileState.NONE) {
            return kotlin.Pair(false, "Programming error, send a bug report!")
        }

        val (index_y, index_x) = this.unflatten_index(index)

        return if (this.game_board[index_y][index_x] != TileState.NONE) {
            Pair(false, "Tile already taken!")
        } else {
            this.game_board[index_y][index_x] = move
            Pair(true, "You've made your move!")
        }
    }
}
