package com.example.tictactoe

import android.content.Context
import android.os.Bundle
import android.view.View
import android.content.Intent
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar

import androidx.appcompat.app.AppCompatActivity

class HallOfFame : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.hall_of_fame)

        val preferences = applicationContext.getSharedPreferences("HALL_OF_FAME", Context.MODE_PRIVATE)
        findViewById<TextView>(R.id.vs_ai).text = "Player vs AI:\n${preferences.getString("VS_AI_SCORE", "No AI score yet")}"
        findViewById<TextView>(R.id.vs_player).text = "Player vs Player:\n${preferences.getString("VS_PLAYER_SCORE", "No multiplayer score yet")}"
    }


}