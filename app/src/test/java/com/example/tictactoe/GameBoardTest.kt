package com.example.tictactoe

import org.junit.Test
import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}

class BoardTest {
    @Test
    fun making_valid_move_is_possible() {
        val b = Board()
        val (move_successful, _) = b.try_making_move(1, TileState.CROSS)
        assert(move_successful)
    }

    @Test
    fun making_move_on_a_taken_tile_is_not_possible() {
        val b = Board()
        b.try_making_move(2, TileState.CROSS)

        val (move_successful, message) = b.try_making_move(2, TileState.CIRCLE)
        assertFalse(move_successful)
        assert(message.isNotEmpty())  // Check if there's any error message for the user
    }

    @Test
    fun test_victory_empty_board() {
        val b = Board()
        assert(b.try_get_winner() == TileState.NONE)
    }

    @Test
    fun test_victory_nonempty_board_without_victory_conditions() {
        val b = Board()
        b.try_making_move(1, TileState.CIRCLE)
        b.try_making_move(2, TileState.CROSS)
        b.try_making_move(3, TileState.CROSS)
        b.try_making_move(4, TileState.CIRCLE)
        b.try_making_move(5, TileState.CROSS)

        assert(b.try_get_winner() == TileState.NONE)
    }

    @Test
    fun test_victory_type_horizontal_line() {
        val b = Board()
        for (i in 0..4) {
            assert(b.try_get_winner() == TileState.NONE)
            val (move_successful, _) = b.try_making_move(i, TileState.CIRCLE)
            assert(move_successful)
        }

        assert(b.try_get_winner() == TileState.CIRCLE)
    }

    @Test
    fun test_victory_type_vertical_line() {
        val b = Board()
        for (i in 0..4) {
            assert(b.try_get_winner() == TileState.NONE)
            val (move_successful, _) = b.try_making_move(i* 10, TileState.CROSS)
            assert(move_successful)
        }

        assert(b.try_get_winner() == TileState.CROSS)
    }

    @Test
    fun test_victory_diagonal() {
        val b = Board()
        var position = 1
        for (i in 0..4) {
            assert(b.try_get_winner() == TileState.NONE)
            val (move_successful, _) = b.try_making_move(position, TileState.CROSS)
            position += 11
            assert(move_successful)
        }
        assert(b.try_get_winner() == TileState.CROSS)
    }

    @Test
    fun test_victory_anti_diagonal() {
        val b = Board()
        var position = 9

        for (i in 0..5) {
            assert(b.try_get_winner() == TileState.NONE)
            val (move_successful, _) = b.try_making_move(position, TileState.CROSS)
            position += 9
            assert(move_successful)
        }
        assert(b.try_get_winner() == TileState.CROSS)
    }
}